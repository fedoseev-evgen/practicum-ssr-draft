import path from 'path';
import fs from 'fs';
import express from 'express';
import React from 'react';
import ReactDomServer from 'react-dom/server';
import App from'./App';

const users = [
    'Коля',
    'Петя',
    'Сережа',
    'Леша',
    'Жека'
];
const template = fs.readFileSync(path.join(__dirname, '../build/index.html'), {
    encoding: 'utf8',
});
const app = express();
app.get('/', (req, res) => {
    const appInitialState = {users};
    const html = ReactDomServer.renderToString(<App {...appInitialState} />);
    const response = template
        .replace(
            '<div id="root"></div>',
            `<div id="root">${html}</div>
<script>
  window.__APP_INITIAL_STATE__ = ${JSON.stringify(appInitialState)}
</script>`,
        );
    res.send(response);
});

app.use(express.static(path.join(__dirname, '../build')));

app.listen(3000, () => {
    console.log('Сервер запущен на порту 3000');
});