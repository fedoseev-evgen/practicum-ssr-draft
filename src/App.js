import React from 'react';
import './App.css';

function App({users}) {
  return (
      <div className="App">
        <header className="App-header">
            <ul>
                {users.map((user, i) => <li key={i}>{user}</li>)}
            </ul>
            <p>
              Привет мир!
            </p>
            <button onClick={() => alert('Жмякнул :)')}>Жмяк</button>
        </header>
      </div>
  );
}

export default App;
